import standardSlugify from 'standard-slugify';

export default {
	name: 'episodes',
	type: 'document',
	title: 'Episodes',
	__experimental_actions: ['update', /* 'create', 'delete', */ 'publish'],
	fields: [
		{
			name: 'title',
			type: 'string',
			title: 'Title',
			validation: Rule => Rule.required()
		},
		{
			name: 'slug',
			type: 'slug',
			title: 'Slug',
			validation: Rule => Rule.required(),
			options: {
				source: doc => `/${doc.title}`,
				slugify: input =>
					standardSlugify(input, {
						keepCase: false,
						replacements: {
							'/': '/'
						}
					}),
				maxLength: 120
			}
		},
		{
			name: 'homePageSeo',
			title: 'Home Page SEO',
			type: 'seo',
			options: {
				collapsable: true
			}
		},
		{
			type: 'textImage',
			name: 'introTextImage',
			options: {
				collapsable: true
			}
		},
		{
			type: 'richText',
			name: 'episodeBody',
		},
		{
			name: 'liveSet',
			type: 'boolean',
			title: 'Live recording',
		},
		{
			name: 'yearMix',
			type: 'boolean',
			title: 'Year mix',
		},
		{
			title: 'Guests',
			name: 'guests',
			type: 'array',
			of: [
				{
					type: 'string',
					name: 'guest'
				}
			],
		},
		{
			title: 'Genres',
			name: 'genres',
			type: 'array',
			of: [
				{
					type: 'string',
					name: 'genre'
				}
			],
		},
	],
	preview: {
		select: {
			title: 'title',
		}
	}
};

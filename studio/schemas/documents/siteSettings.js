import standardSlugify from 'standard-slugify';

export default {
	name: 'siteSettings',
	type: 'document',
	title: 'Site',
	__experimental_actions: ['update', /* 'create', 'delete', */ 'publish'],
	fields: [
		{
			name: 'title',
			type: 'string',
			title: 'Title',
			validation: Rule => Rule.required()
		},
		{
			name: 'homePageSeo',
			title: 'Home Page SEO',
			type: 'seo',
			options: {
				collapsable: true
			}
		},
		{
			title: 'Current episode',
			name: 'currentEpisode',
			// A reference is a way to point to another document
			type: 'reference',
			// This reference is only allowed to point to a document of the type person,
			// we could list more types, but let's keep this simple:
			to: [{type: 'episodes'}]
		}
	],
	preview: {
		select: {
			title: 'title',
		}
	}
};

export default {
	name: 'years',
	type: 'document',
	title: 'Years',
	__experimental_actions: ['update', /* 'create', 'delete', */ 'publish'],
	fields: [
		{
			name: 'title',
			type: 'string',
			title: 'Title',
			validation: Rule => Rule.required()
		},
		{
			name: 'slug',
			type: 'slug',
			title: 'Slug',
			validation: Rule => Rule.required()
		},
		{
			name: 'description',
			type: 'text',
			title: 'Default description',
			description:
				'The description used on the front page and as a default fallback.'
		},
		{
			name: 'homePageSeo',
			title: 'Home Page SEO',
			type: 'seo',
			options: {
				collapsable: true
			}
		},
		{
			name: 'color',
			type: 'string',
			title: 'Color',
		},
		{
			name: 'drawerColor',
			type: 'string',
			title: 'Drawer Color',
		},
		{
			title: 'Episodes in year',
			name: 'episodes',
			type: 'array',
			of: [
				{
					type: 'reference',
					to: [{type: 'episodes'}]
				}
			],
		}
	],
	preview: {
		select: {
			title: 'title',
		}
	}
};

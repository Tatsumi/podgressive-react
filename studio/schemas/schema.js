// First, we must import the schema creator
import createSchema from 'part:@sanity/base/schema-creator'

// Then import schema types from any plugins that might expose them
import schemaTypes from 'all:part:@sanity/base/schema-type'

// Import Documents
import siteSettings from './documents/siteSettings';
import years from './documents/years';
import episodes from './documents/episodes';

// Import Objects
import bodyPortableText from './objects/bodyPortableText';
import imageObject from './objects/imageObject';
import richText from './objects/richText';
import seo from './objects/seo';
import textImage from './objects/textImage';

// Then we give our schema to the builder and provide the result to Sanity
export default createSchema({
  // We name our schema
  name: 'site',
  // Then proceed to concatenate our document type
  // to the ones provided by any plugins that are installed
  types: schemaTypes.concat([
		siteSettings,
		years,
		episodes,
		// Objects
		bodyPortableText,
		imageObject,
		richText,
		seo,
		textImage,
  ])
})

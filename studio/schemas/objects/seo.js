export default {
  name: 'seo',
  type: 'object',
  fields: [
    {
      name: 'seoTitle',
      title: 'SEO Title',
      description: "Used to override this document's title for SEO",
      type: 'string'
    },
    {
      name: 'description',
      type: 'text',
      title: 'Description',
      description:
        'The text under the page title in the Google result | Max: 160',
      validation: Rule => Rule.required().max(160)
    },
    {
      name: 'ogImage',
      title: 'Open Graph Image',
      type: 'imageObject'
    }
  ]
};

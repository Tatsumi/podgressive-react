import { toPlainText } from '../../../src/lib/helpers';

export default {
  name: 'textImage',
  type: 'object',
  title: 'Text Image',
  fields: [
    {
      name: 'image',
      title: 'Image',
      type: 'image',
      options: {
        hotspot: true
      },
    },
    {
      name: 'alt',
      type: 'string',
      title: 'Alternative text',
      description: 'Important for SEO and accessiblity.'
    },
    {
      name: 'textContent',
      title: 'Text Content',
      type: 'bodyPortableText',
    }
  ],
  preview: {
    select: {
      title: 'textContent'
    },
    prepare(selection) {
      const { title } = selection;
      return {
        title: toPlainText(title)
      };
    }
  }
};

import S from '@sanity/desk-tool/structure-builder';

const hiddenDocTypes = listItem =>
	![
		'years',
		'episodes',
		'siteSettings',
	].includes(listItem.getId());

export default () =>
	S.list()
		.title('Content')
		.items([
			S.listItem()
				.title('Settings')
				.child(
					S.editor()
						.id('siteSettings')
						.schemaType('siteSettings')
						.documentId('siteSettings')
				),
			S.listItem()
				.title('Years')
				.child(S.documentTypeList('years').title('Years')),
			S.listItem()
				.title('Episodes')
				.child(S.documentTypeList('episodes').title('Episodes')),
			// This returns an array of all the document types
			// defined in schema.js. We filter out those that we have
			// defined the structure above
			...S.documentTypeListItems().filter(hiddenDocTypes)
		]);

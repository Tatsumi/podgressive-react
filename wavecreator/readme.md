How to create audiowaveform json objects
----
1 Open terminal and cd to waveform/build folder <br>
2 Move mp3 file to this folder<br>
3 Run <code>audiowaveform -i podgressive_ep029_192kbps.mp3 -o podgressive_ep029.json --pixels-per-second 20 --bits 8</code> and replace episode name.<br>
4 Normalize peaks by <code>python scale-json.py podgressive_ep029.json</code>
5 Done, upload file to peaks folder in server.

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash';

import getSiteSettings from '../actions/getSiteSettings';
import currentlyPlaying from '../actions/currentlyPlaying';

import Layout from '../components/templates/Layout';
import Body from '../components/templates/Body';

class Index extends React.Component {
	changeEpisodeCb(episode) {
		this.props.currentlyPlaying(episode, true);
	}

  componentDidMount() {
    this.props.getSiteSettings();
  }

  render() {
    const { siteSettings, currentEpisode, mounted } = this.props;

    // Site
		const episodeSeoDescription = get(siteSettings, 'homePageSeo.description', null);
		const episodeSeoImage = get(siteSettings, 'homePageSeo.ogImage.image', null);

		// Episode
		const title = get(currentEpisode, 'title', '');
		const intro = get(currentEpisode, 'introTextImage.textContent', null);
		const introImage = get(currentEpisode, 'introTextImage.image', null);
		const introImageAlt = get(currentEpisode, 'introTextImage.alt', null);
		const episodeBody = get(currentEpisode, 'episodeBody.textContent', null);
		const genres = get(currentEpisode, 'genres', null);
		const guests = get(currentEpisode, 'guests', null);
		const liveSet = get(currentEpisode, 'liveSet', null);
		const yearMix = get(currentEpisode, 'yearMix', null);

    const seo = {
			description: episodeSeoDescription,
			image: episodeSeoImage,
		};

		return (
			<Layout seo={seo}>
				<Body
					title={title}
					intro={intro}
					mounted={mounted}
					introImage={introImage}
					introImageAlt={introImageAlt}
					episodeBody={episodeBody}
					genres={genres}
					guests={guests}
					liveSet={liveSet}
					yearMix={yearMix}
					playPressed={(episode) => this.changeEpisodeCb(episode)}
				/>
			</Layout>
		)
  }
}

const mapStateToProps = ({ siteSettings, currentEpisode, yearsFetched, mounted }) => {
	return {
		siteSettings: siteSettings.settings,
		currentEpisode: currentEpisode.current,
		years: yearsFetched.years,
		mounted: mounted.episode,
	}
};

const mapDispachToProps = (dispatch) => {
	return bindActionCreators({
		getSiteSettings,
		currentlyPlaying,
	}, dispatch);
};

export default connect(mapStateToProps, mapDispachToProps)(Index);

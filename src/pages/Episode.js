import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { get } from 'lodash';

import getSiteSettings from '../actions/getSiteSettings';
import getEpisodeBySlug from '../actions/getEpisodeBySlug';
import currentlyPlaying from '../actions/currentlyPlaying';
import Layout from '../components/templates/Layout';
import Body from '../components/templates/Body';


class Episode extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			currentPage: null,
			playing: null,
		}
	}
	componentDidMount() {
		const { match } = this.props;

		this.props.getSiteSettings(match.params.episode);

		this.setState({
			currentPage: match.params.episode,
		});
	}

	changeEpisodeCb(episode) {
		this.props.currentlyPlaying(episode, true);
	}

	render() {

		const { currentEpisode, match, mounted } = this.props;
		const { currentPage } = this.state;

		if(match.params.episode !== currentPage) {
			this.props.getEpisodeBySlug(match.params.episode);

			this.setState({
				currentPage: match.params.episode,
			});
		}

		// Episode
		const title = get(currentEpisode, 'title', '');
		const episodeSeoTitle = get(currentEpisode, 'homePageSeo.seoTitle', null);
		const episodeSeoDescription = get(currentEpisode, 'homePageSeo.description', null);
		const episodeSeoImage = get(currentEpisode, 'homePageSeo.ogImage.image', null);
		const intro = get(currentEpisode, 'introTextImage.textContent', null);
		const introImage = get(currentEpisode, 'introTextImage.image', null);
		const introImageAlt = get(currentEpisode, 'introTextImage.alt', null);
		const episodeBody = get(currentEpisode, 'episodeBody.textContent', []);
		const genres = get(currentEpisode, 'genres', null);
		const guests = get(currentEpisode, 'guests', null);
		const liveSet = get(currentEpisode, 'liveSet', null);
		const yearMix = get(currentEpisode, 'yearMix', null);

		const seo = {
			title: `${title}${episodeSeoTitle === '' ? ` - ${episodeSeoTitle}` : ''}`,
			description: episodeSeoDescription,
			image: episodeSeoImage,
			slug: '/'
		};

		return (
			<Layout seo={seo} props={this.props} currentYear={match.params.year}>
				<Body
					title={title}
					intro={intro}
					mounted={mounted}
					introImage={introImage}
					introImageAlt={introImageAlt}
					episodeBody={episodeBody}
					genres={genres}
					guests={guests}
					liveSet={liveSet}
					yearMix={yearMix}
					playPressed={(episode) => this.changeEpisodeCb(episode)}
				/>
			</Layout>
		)
	}
}

const mapStateToProps = ({ siteSettings, currentEpisode, yearsFetched, mounted, change }) => {
	return {
		siteSettings: siteSettings.settings,
		currentEpisode: currentEpisode.current,
		years: yearsFetched.years,
		mounted: mounted.episode,
		change: change.toEpisode,
	}
};

const mapDispachToProps = (dispatch) => {
	return bindActionCreators({
		getSiteSettings,
		getEpisodeBySlug,
		currentlyPlaying,
	}, dispatch);
};

export default connect(mapStateToProps, mapDispachToProps)(Episode);

import client from '../lib/sanityConfig';
import { get } from 'lodash';

const getSiteSettings = (currentPageEpisode) => {
	return dispatch => {

		const query = '*[_type == "siteSettings"][0] {title, currentEpisode, description, homePageSeo}';

		client.fetch(query).then(site => {
			const currentEpisodeRef = get(site, 'currentEpisode._ref');
			let pageEpisodeQuery = `*[_type == "episodes" && _id == "${currentEpisodeRef}"][0]`;

			if (currentPageEpisode) {
				pageEpisodeQuery = `*[slug.current == "/${currentPageEpisode}/"][0]`;
			}

			client.fetch(pageEpisodeQuery).then(episode => {
				dispatch({
					type: 'CURRENT_EPISODE_FULFILLED',
					payload: episode,
				});
			});

			dispatch({
				type: 'SITE_SETTINGS_FULFILLED',
				payload: site,
			});
		});
	}
};

export default getSiteSettings;

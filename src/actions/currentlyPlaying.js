const currentlyPlaying = (title, isPlaying = false) => {
	const data = {
		title: title,
		isPlaying,
	};
	return dispatch => {
		dispatch({
			type: 'MOUNTED_EPISODE',
			payload: data,
		});
	}
};

export default currentlyPlaying;

import client from '../lib/sanityConfig';

const getYears = () => {
	return dispatch => {

		const queryYears = `*[_type == "years"] | order(title desc, title) {
			_id,
			color,
			drawerColor,
			description,
			slug,
			title,
			episodes[]->,
		}`;

		client.fetch(queryYears).then(years => {
			dispatch({
				type: 'YEARS_FULFILLED',
				payload: years,
			});
		});
	}
};

export default getYears;

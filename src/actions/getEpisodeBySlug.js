import client from '../lib/sanityConfig';

const getEpisodeBySlug = (slug) => {
	return dispatch => {
		const queryEpisode = `*[slug.current == "/${slug}/"][0]`;

		client.fetch(queryEpisode).then(episode => {
			dispatch({
				type: 'CURRENT_EPISODE_FULFILLED',
				payload: episode,
			});
		});
	}
};

export default getEpisodeBySlug;

const siteSettings = (state = {}, action) => {
	switch(action.type) {
		case 'SITE_SETTINGS_FULFILLED':
			return { ...state, settings: action.payload };
		case 'SITE_SETTINGS_REJECTED':
			return { ...state, settings: action.error };
		default:
			return state;
	}
};

export default siteSettings;

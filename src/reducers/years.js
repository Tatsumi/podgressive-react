const yearsFetched = (state = {}, action) => {
	switch(action.type) {
		case 'YEARS_FULFILLED':
			return { ...state, years: action.payload };
		default:
			return state;
	}
};

export default yearsFetched;

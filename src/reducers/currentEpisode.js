const episode = (state = {}, action) => {
	switch(action.type) {
		case 'CURRENT_EPISODE_FULFILLED':
			return { ...state, current: action.payload };
		case 'CURRENT_EPISODE_REJECTED':
			return { ...state, current: action.error };
		default:
			return state;
	}
};

export default episode;

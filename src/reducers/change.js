const change = (state = {}, action) => {
	switch(action.type) {
		case 'CHANGE_EPISODE':
			return { ...state, toEpisode: action.payload };
		default:
			return state;
	}
};

export default change;

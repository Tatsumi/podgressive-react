import { combineReducers } from 'redux';
import siteSettings from './getSiteSettings';
import currentEpisode from './currentEpisode';
import yearsFetched from './years';
import episodesFetched from './episodes';
import mounted from './mounted';
import change from './change';

export default combineReducers({
	siteSettings,
	currentEpisode,
	yearsFetched,
	episodesFetched,
	mounted,
	change
});

const episodesFetched = (state = {}, action) => {
	switch(action.type) {
		case 'EPISODES_FULFILLED':
			return { ...state, episodes: action.payload };
		default:
			return state;
	}
};

export default episodesFetched;

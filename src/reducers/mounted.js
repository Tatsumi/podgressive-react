const mounted = (state = {}, action) => {
	switch(action.type) {
		case 'MOUNTED_EPISODE':
			return { ...state, episode: action.payload };
		default:
			return state;
	}
};

export default mounted;

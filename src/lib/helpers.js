export function mapEdgesToNodes(data) {
	if (!data.edges) return [];
	return data.edges.map(edge => edge.node);
}

export function buildImageObj(source) {
	return {
		asset: {
			_ref: source.asset && (source.asset._ref || source.asset._id)
		},
		// Doesn't add these props if they are empty
		...(source.crop && { crop: source.crop }),
		...(source.hotspot && { hotspot: source.hotspot })
	};
}

export function toPlainText(blocks) {
	return blocks
		.map(block => {
			if (block._type !== 'block' || !block.children) {
				return '';
			}
			return block.children.map(child => child.text).join('');
		})
		.join('\n\n');
}

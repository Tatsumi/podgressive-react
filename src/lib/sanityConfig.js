const sanityClient = require('@sanity/client');

const client = sanityClient({
	projectId: 'ar4gtaz3',
	dataset: 'production',
	useCdn: true // `false` if you want to ensure fresh data
});

export default client;

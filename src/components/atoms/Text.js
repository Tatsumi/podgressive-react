import React from 'react';
import styled from 'styled-components';

const Span = styled.span`
  font-family: 'Open Sans', sans-serif;
	font-weight: ${props => props.light ? '400' : '600'};
	font-size: ${props => props.small ? props.theme.font.sm.fontSize : props.theme.font.text.fontSize}
	line-height: ${props => props.small ? props.theme.font.sm.lineHeight : props.theme.font.text.lineHeight}
	text-transform: ${props => props.uppercase && 'uppercase'};
	word-break: break-word;
	hyphens: auto;
	
	${props => props.linkText && `
		margin-bottom: 3px;
		&:hover {
			border-bottom: 3px solid black;
		}
	`}

	@media (max-width: ${props => props.theme.breakpoints.md}px) {
		font-size: ${props => props.theme.font.sm.fontSize}
		line-height: ${props => props.theme.font.sm.lineHeight}
  }
`;

const P = styled.p`
	margin-bottom: 2rem;
  font-family: 'Open Sans', sans-serif;
	font-weight: ${props => props.light ? '400' : '600'};
	font-size: ${props => props.small ? props.theme.font.sm.fontSize : props.theme.font.text.fontSize}
	line-height: ${props => props.small ? props.theme.font.sm.lineHeight : props.theme.font.text.lineHeight}
	text-transform: ${props => props.uppercase && 'uppercase'};
	word-break: break-word;
	hyphens: auto;

	${props => props.linkText && `
		margin-bottom: 3px;
		&:hover {
			border-bottom: 3px solid black;
		}
	`}

	@media (max-width: ${props => props.theme.breakpoints.md}px) {
		font-size: ${props => props.theme.font.sm.fontSize}
		line-height: ${props => props.theme.font.sm.lineHeight}
  }
`;

const Text = ({ paragraph = null, light = null, uppercase = null, small = null, linkText = null, children }) => {
	return paragraph ?
		<P small={small} light={light} uppercase={uppercase} linkText={linkText}>{ children }</P> :
		<Span small={small} light={light} uppercase={uppercase} linkText={linkText}>{ children }</Span>;
};

export { Text };

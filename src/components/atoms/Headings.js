import React from 'react';
import styled, {css} from 'styled-components';

const Heading1 = styled.h1`
  font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	font-size: ${props => props.theme.font.lg.fontSize};
	line-height: ${props => props.theme.font.lg.lineHeight};
	text-transform: uppercase;
	margin-bottom: 3rem;

	@media (max-width: ${props => props.theme.breakpoints.md}px) {
		font-size: ${props => props.theme.font.lg.fontSizeSmall};
    line-height: ${props => props.theme.font.lg.lineHeightSmall};
  }
`;

const H1 = ({ children }) => {
	return (
		<Heading1>
			{ children }
		</Heading1>
	)
};

const Heading2 = styled.h2`
	margin-bottom: 0.5rem;
  font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	font-size: ${props => props.theme.font.md.fontSize};
	line-height: ${props => props.theme.font.lg.lineHeight};
	text-transform: uppercase;

	@media (max-width: ${props => props.theme.breakpoints.md}px) {
		font-size: ${props => props.theme.font.md.fontSizeSmall};
    line-height: ${props => props.theme.font.lg.lineHeightSmall};
  }
  
  ${props => props.normalize && css`
  	margin-bottom: 2rem;
		font-family: 'Open Sans', sans-serif;
		font-weight: 400;
		font-size: ${props => props.theme.font.text.fontSize}
		line-height: ${props => props.theme.font.text.lineHeight}
		word-break: break-word;
		hyphens: auto;
		text-transform: initial;
	
		@media (max-width: ${props => props.theme.breakpoints.md}px) {
			font-size: ${props => props.theme.font.sm.fontSize}
			line-height: ${props => props.theme.font.sm.lineHeight}
		}
  `}
`;
const H2 = ({ normalize, children }) => {
	return (
		<Heading2 normalize={normalize}>
			{ children }
		</Heading2>
	)
};

const Heading3 = styled.h3`
	margin-bottom: 0.5rem;
  font-family: 'Open Sans', sans-serif;
	font-weight: 600;
	font-size: ${props => props.theme.font.md.fontSize};
	line-height: ${props => props.theme.font.lg.lineHeight};
	text-transform: uppercase;

	@media (max-width: ${props => props.theme.breakpoints.md}px) {
		font-size: ${props => props.theme.font.md.fontSizeSmall};
    line-height: ${props => props.theme.font.lg.lineHeightSmall};
  }
`;

const H3 = ({ children }) => {
	return (
		<Heading3>
			{ children }
		</Heading3>
	)
};

export { H1, H2, H3 };

import React from 'react';
import styled, {css}  from 'styled-components';

const Ol = styled.ol`
	list-style: decimal-leading-zero;
	margin-left: 3.4rem;
	margin-bottom: 4rem;
	padding: 0;
`;

const Ul = styled.ul`
	list-style: disc;
	padding: 0;
	margin-left: 2.5rem;
	margin-bottom: 4rem;
	
	${props => props.normal && css`
  	list-style: none;
		padding: 0;
		margin-left: 0;
		margin-bottom: 1rem;
  `}
`;

const Li = styled.li`
	margin-bottom: 1rem;
  font-family: 'Open Sans', sans-serif;
	font-weight: 400;
	font-size: ${props => props.theme.font.text.fontSize}
	line-height: ${props => props.theme.font.text.lineHeight}
	word-break: break-word;
	hyphens: auto;

	@media (max-width: ${props => props.theme.breakpoints.md}px) {
		font-size: ${props => props.theme.font.sm.fontSize}
		line-height: ${props => props.theme.font.sm.lineHeight}
  }
  
  ${props => props.small && css`
  	margin-bottom: 0;
  	font-size: ${props => props.theme.font.sm.fontSize}
		line-height: ${props => props.theme.font.sm.lineHeight}
  `}
  
  ${props => props.strong && css`
  	font-weight: 600;
  `}
`;

const List = ({ number = null, normal = null, children }) => {
	return number ?
		<Ol>{children}</Ol> :
		<Ul normal={normal}>{children}</Ul>;
};

export { List, Li };

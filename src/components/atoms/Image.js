import React from 'react';
import {get} from 'lodash';
import {imageUrlFor} from '../../lib/image-url';
import {buildImageObj} from '../../lib/helpers';


const Image = ({image, alt}) => {
	const asset = get(image, 'asset', null);

	return asset ? (
		<picture>
			<source
				type="image/webp"
				media="(min-width: 1008px)"
				srcSet={
					imageUrlFor(buildImageObj(image)).width(1920).height(1080).format('webp').quality(80)
				}
			/>
			<source
				media="(min-width: 1008px)"
				srcSet={imageUrlFor(buildImageObj(image)).format('jpg')}
			/>
			<img
				srcSet={imageUrlFor(buildImageObj(image)).width(1920 / 2).height(1080 / 2).format('jpg')}
				alt={alt}
				style={{maxWidth: '100%'}}
			/>
		</picture>
	) : null;
};
Image.defaultProps = {
	alt: ''
};

export default Image;

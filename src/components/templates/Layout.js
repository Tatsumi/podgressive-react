import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { Helmet } from "react-helmet";
import styled from 'styled-components';
import { get } from 'lodash';

import getYears from '../../actions/getYears';
import currentlyPlaying from '../../actions/currentlyPlaying';

import Header from '../molecyles/Header/Header';
import Menu from '../molecyles/Menu/Menu';
import {imageUrlFor} from '../../lib/image-url';
import {buildImageObj} from '../../lib/helpers';

const Wrapper = styled.main`
  max-width: 1200px;
	margin: 0 auto;
	padding: 8rem;

	@media (max-width: 1200px) {
    padding: 4rem;
  }
`;

const theme = {
	breakpoints: {
		xs: 0,
		sm: 576,
		md: 768,
		lg: 992,
		xl: 1200
	},
	font: {
		lg: {
			fontSize: '3.2rem',
			lineHeight: '4.3rem',
			fontSizeSmall: '2.8rem',
			lineHeightSmall: '3.3rem',
		},
		md: {
			fontSize: '2.2rem',
			lineHeight: '3.0rem',
		},
		sm: {
			fontSize: '1.6rem',
			lineHeight: '2.5rem',
		},
		text: {
			fontSize: '2.0rem',
			lineHeight: '3.2rem',
		},
	}
};

class Layout extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			menuState: false,
		}
	}

	componentDidMount() {
		document.addEventListener('keyup', (e) => {
			if(e.code === 'Tab') {
				document.body.classList.add('tabbing');
			}
		})
	}

	render() {
		const { seo, years, children, currentEpisode, currentYear } = this.props;
		const { title, description, slug, image } = seo;
		const { menuState } = this.state;
		const currentEpisodeTitle = get(currentEpisode, 'title', null);

		if(!years) {
			this.props.getYears();
		}

		return (
			<ThemeProvider theme={theme}>
				<Helmet
					title={title ? `${title && `${title} | `}Podgressive Podcast` : 'Podgressive Podcast'}
					meta={[
					{ name: 'description', content: description || '' },
					{ property: 'og:title', content: title ? `${title && `${title} | `}Podgressive Podcast` : 'Podgressive Podcast'},
					{ property: 'og:type', content: "website" },
					{ property: 'og:url', content: `https://podgressive.com${slug}`},
					{ property: 'og:description', content: description || '' },
					{ property: 'og:image', content: image && imageUrlFor(buildImageObj(image)).width(1200).height(628).format('jpg').quality(80)},
					]}
				/>
				<Menu
					menuState={menuState}
					years={years}
					currentYear={currentYear}
					closeMenu={(colour) => this.setState({menuState: !menuState, menuTriggeredYearColour: colour})}
				/>
				<Header
					menuState={menuState}
					title={title}
					currentEpisode={currentEpisodeTitle}
					years={years}
					currentYear={currentYear}
					openMenu={() => this.setState({menuState: !menuState})} {...this.props}
					updateMountedState={(title, isPlaying, callbackDone) => this.props.currentlyPlaying(title, isPlaying, callbackDone)}
				/>
				<Wrapper>
					{ children }
				</Wrapper>
			</ThemeProvider>
		)
	}
}

Layout.defaultProps = {
	seo: {
		title: null,
		description: null,
		slug: null,
		image: null,
	}
};

Layout.propTypes = {
	seo: PropTypes.object,
};


const mapStateToProps = ({ yearsFetched, siteSettings, currentEpisode, mounted }) => {
	return {
		years: yearsFetched.years, // Also containing episodes. TODO: Refactor naming.
		siteSettings: siteSettings.settings,
		currentEpisode: currentEpisode.current,
		mounted: mounted.episode,
	}
};

const mapDispachToProps = (dispatch) => {
	return bindActionCreators({
		getYears,
		currentlyPlaying,
	}, dispatch);
};

export default connect(mapStateToProps, mapDispachToProps)(Layout);

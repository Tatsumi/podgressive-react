import React from 'react';
import styled from 'styled-components';
import Grid from 'styled-components-grid';
import {get} from 'lodash';
import TextMolecyle from '../molecyles/TextMolecyle';
import GridUnit from '../molecyles/GridUnit';
import { H1, H3 } from '../atoms/Headings';
import { List, Li } from '../atoms/List';
import {Text} from '../atoms/Text';
import checked from '../../images/checked.svg';
import Image from '../atoms/Image';
import play from '../../images/play.svg';

const CheckedText = styled.span`
	margin-left: 10px;
	vertical-align: text-bottom;
`;

const Intro = styled.section`
	padding-bottom: 8rem;
	margin-bottom: 8rem;
	border-bottom: 3px solid black;
	@media (max-width: 768px) {
		padding-bottom: 4rem;
		margin-bottom: 4rem;	
  }
`;

const Interact= styled.div`
	margin-bottom: 30px;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	
	button {
		margin-right: 3rem;
		background-color: transparent;
		border: none;
		padding: 0;
		cursor: pointer;
	}
`;

const Body = ({title, intro, mounted, introImage, introImageAlt, liveSet, yearMix, guests, genres, episodeBody, playPressed}) => {
	const mountedTitle = get(mounted, 'title', null);
	const pressedButton = () => {
		playPressed(title);
	};

	return (
		<>
			<Intro id="mainContent">
				<Grid halign="left">
					<GridUnit column size={{xs: 1, md: 0.6}}>
						<H1>Podgressive {title}</H1>
						{intro && <TextMolecyle blocks={intro} isHeading />}
						{title !== mountedTitle && (
							<Interact>
								<button onClick={() => pressedButton()}>
									<img src={play} alt="play"/>
								</button>
								<Text uppercase>{title}</Text>
							</Interact>
						)}
					</GridUnit>
					{introImage && (
						<GridUnit column size={{xs: 1, md: 0.4}} style={{padding: '2rem'}}>
							<Image image={introImage} alt={introImageAlt}/>
						</GridUnit>
					)}
				</Grid>
			</Intro>
			<section>
				<Grid halign="left">
					{(guests || genres || liveSet || yearMix) && (
						<GridUnit column size={{xs: 1, md: 0.3}}>
							<H3>Metadata</H3>
							<List normal>
								{guests && (
									<>
									<Li small strong>{guests.length > 1 ? 'Guest mixes by' : 'Guest mix by'}</Li>
									<List normal>
										{guests.map((guest, key) => (
											<Li small key={`guest-tag-${key}-${guest}`}>
												<img src={checked} alt="checked icon" width="15" height="15"/>
												<CheckedText>{guest}</CheckedText>
											</Li>
										))}
									</List>
									</>
								)}
								{genres && (
									<>
									<Li small strong>Genre</Li>
									<List normal>
										{genres.map((genre, key) => (
											<Li small key={`genre-tag-${key}-${genre}`}>
												<img src={checked} alt="checked icon" width="15" height="15"/>
												<CheckedText>{genre}</CheckedText>
											</Li>
										))}
									</List>
									</>
								)}
								{liveSet || yearMix ? (
									<>
									<Li small strong>Misc</Li>
									{liveSet && <Li small><img src={checked} alt="checked icon" width="15" height="15"/><CheckedText>Live set</CheckedText></Li>}
									{yearMix && <Li small><img src={checked} alt="checked icon" width="15" height="15"/><CheckedText>Year mix</CheckedText></Li>}
									</>
								) : ''}
							</List>
						</GridUnit>
					)}
					<GridUnit column size={{xs: 1, md: 0.7}}>
						<article>
							{episodeBody && <TextMolecyle blocks={episodeBody} />}
						</article>
					</GridUnit>
				</Grid>
			</section>
		</>
	)
};

Body.defaultProps = {
	guests: null,
	genres: null,
	episodeBody: null,
	liveSet: false,
	yearMix: false,
};

export default Body;


import React from 'react';
import clientConfig from '../../../lib/sanityConfig';
import BlockContent from '@sanity/block-content-to-react';

import { Text } from '../../../components/atoms/Text';
import { List, Li } from '../../../components/atoms/List';
import { H1, H2, H3 } from '../../../components/atoms/Headings';

const BlockRenderer = (isHeading, props) => {
	const style = props.node.style || 'number';

	if(isHeading) {
		return <H2 normalize>{props.children}</H2>
	} else {
		switch(style) {
			case 'h1':
				return <H1>{props.children}</H1>
			case 'h2':
				return <H2>{props.children}</H2>
			case 'h3':
				return <H3>{props.children}</H3>
			case 'p':
			case 'normal':
				return <Text paragraph light>{props.children}</Text>
			case 'span':
				return <Text light>{props.children}</Text>
			default:
				return <Text paragraph light>{props.children}</Text>
		}
	}
};

const list = (props) => {
	return props.type === 'bullet' ? <List>{props.children}</List> : <List number>{props.children}</List>
};

const listItem = (props) => {
	return <Li>{props.children}</Li>
};

const TextMolecyle = ({ isHeading = null, blocks }) => (
	<BlockContent
		blocks={blocks}
		serializers={{types: {block: (props) => BlockRenderer(isHeading, props)}, list: (props) => list(props), listItem: (props) => listItem(props)}}
		{...clientConfig}
	/>
);
export default TextMolecyle;

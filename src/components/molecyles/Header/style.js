import styled from 'styled-components';

const Head = styled.header`
	height: 400px;
	padding: 5rem 8rem 2rem;
	background-color: ${props => props.yearColour}
	transition: all 1s ease-in-out;
	@media (max-width: ${props => props.theme.breakpoints.md}px) {
    padding: 4rem 4rem 2rem;
  }
`;

// Logo and breadcrumbs
const InnerContainer = styled.div`
	display: flex;
	justify-content: space-between;
`;

const LogoNavigation = styled.div`
	position: relative;

	@media (max-width: ${props => props.theme.breakpoints.sm}px) {
    img {
    	width: 160px;
    }
  }
`;

const Breadcrumbs = styled.ul`
	display: flex;
	justify-content: flex-start;
	position: absolute;
	bottom: 0.2rem;
	left: 6.5rem;

	li {
		margin-right: 0.5rem;
	}

	a span:hover {
		border-bottom: 3px solid black;
	}

	@media (max-width: ${props => props.theme.breakpoints.sm}px) {
    display: none;
  }
`;

// Hmburger menu
const HamburgerButton = styled.button`
	display: flex;
	align-items: center;
	background-color: transparent;
	border: none;
	cursor: pointer;
	
	i {
		width: 3rem;
		height: 0.3rem;
		margin-left: 1rem;
		background-color: black;
		position: relative;
		
		&:before {
			content: '';
			width: 3rem;
			height: 0.3rem;
			background-color: black;
			position: absolute;
			top: -0.9rem;
			left: 0;
			transition: all 0.2s ease-in-out;
		}
		
		&:after {
			content: '';
			width: 3rem;
			height: 0.3rem;
			background-color: black;
			position: absolute;
			bottom: -0.9rem;
			left: 0;
			transition: all 0.2s ease-in-out;
		}
		@media (max-width: ${props => props.theme.breakpoints.sm}px) {
			top: 1rem;
		}
	}
	
	${props => props.isOpen && `
		position: fixed;
		right: 4rem;
		z-index: 1338;
		i {
			background-color: transparent;
			
			&:before {
				top: 0;
				transform: rotate(45deg);
			}
			
			&:after {
				bottom: 0;
				transform: rotate(-45deg);
			}
		}
	`}
	
	@media (max-width: ${props => props.theme.breakpoints.sm}px) {
		span {
			display: none;
		}    
  }
`;

export { Head, InnerContainer, LogoNavigation, Breadcrumbs, HamburgerButton };

import React, { useState } from 'react';
import { BrowserRouter as Router, Link, useRouteMatch } from 'react-router-dom';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import Player from '../../molecyles/Player';
import { Text } from '../../atoms/Text';
import logo from '../../../images/podgressive-logo.svg';
import { Head, InnerContainer, LogoNavigation, Breadcrumbs, HamburgerButton } from './style';

const Header = ({ title, years, currentYear, currentEpisode, openMenu, menuState, updateMountedState }) => {

	let match = useRouteMatch();
	const [ jumpToContentState, setJumpToContentState ] = useState('visually-hidden');


	const currentYearObj = years && years.filter(obj => {
		return obj.title === currentYear
	});
	const currentYearColor = get(currentYearObj, '[0].color', '#FCCA79')

	return (
		<Head yearColour={currentYearColor}>
			{!menuState && (
				<a
					className={jumpToContentState}
					href="#mainContent"
					title="Jump to content"
					onFocus={() => setJumpToContentState('visible')}
					onBlur={() => setJumpToContentState('visually-hidden')}
				>
					<Text>Jump to content</Text>
				</a>
			)}
			<InnerContainer>
				<LogoNavigation>
					<Router>
						<Link to="/">
							<img src={logo} alt="Podgressive podcast logo" />
						</Link>
					</Router>
					{Object.keys(match.params).length > 0 && (
						<Breadcrumbs>
							{match.params.year && (
								<li>
									<Text small>/ {match.params.year}</Text>
								</li>
							)}
							{currentEpisode && (
								<li>
									<Text small>/ {title ? title :  currentEpisode}</Text>
								</li>
							)}
						</Breadcrumbs>
					)}
				</LogoNavigation>
				<div>
					<HamburgerButton isOpen={menuState} onClick={() => openMenu(!menuState)} aria-expanded={menuState} aria-label="Open/close page menu.">
						{menuState ? <Text>CLOSE</Text> : <Text>MENU</Text>}
						<i>&nbsp;</i>
					</HamburgerButton>
				</div>
			</InnerContainer>
			<Player
				updateMountedStateCb={(title, isPlaying) => updateMountedState(title, isPlaying)}
			/>
		</Head>
	)
};

Header.propTypes = {
	openMenu: PropTypes.func.isRequired,
};

export default Header;

import styled from 'styled-components';

const MenuWrapper = styled.div`
	background: white;
	z-index: 1337;
	position: absolute;
	${props => props.visible && `
		position: fixed;
		top: 0;
		right: 0;
		bottom: 0;
		left: 50%;
		height: 100vh;
		overflow: auto;
	`}
	@media (max-width: ${props => props.theme.breakpoints.lg}px) {
		left: 0;    
	}
`;

const Bg = styled.span`
  height: 100vh;
  width: 50%;
  background-color: ${props => props.color ? props.color : '#FFB8B1'};
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;

  @media (max-width: ${props => props.theme.breakpoints.md}px) {
		position: fixed;
		width: 100%;
	}
`;

const MenuInnerWrapper = styled.div`
  padding: 12rem 4rem 0;
  position: relative;

  @media (max-width: ${props => props.theme.breakpoints.md}px) {
		padding: 8rem 4rem 0;
  }
`;

const Nav = styled.ul`
  margin-top: 2rem;
  z-index: 1;
  
  li {
  	display: block;
  }
`;

const Year = styled.li`
	${props => props.visible && `
		margin: 1rem 0;
	`}
`;

const Episodes = styled.ul`
  position: absolute !important;
  top: 14rem;
  left: calc(50% + 4rem);
 
  li {
  	margin: 0 0 1rem 0;
  }

  @media (max-width: ${props => props.theme.breakpoints.md}px) {
		margin: ${props => props.visible ? '2rem -4rem !important' : '0 -4rem !important'};
		padding: ${props => props.visible ? '4rem !important' : '0 4rem !important'};
		position: static !important;
		background-color: ${props => props.visible ? 'white' : 'transparent'};
	}
`;

const Button = styled.button`
	padding: 0;
	margin-bottom: 0.5rem;
  background-color: transparent;
  border: none;
	border-bottom: ${props => props.selected ? '3px solid black' : '3px solid transparent'};
	cursor: pointer;
	
	&:hover {
		border-bottom-color: black;
	}
`;

export { MenuWrapper, Bg, MenuInnerWrapper, Nav, Year, Episodes, Button};

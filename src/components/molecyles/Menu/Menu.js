import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { get } from 'lodash';

import { MenuWrapper, Bg, MenuInnerWrapper, Nav, Year, Episodes, Button } from './style';
import { H2 } from '../../atoms/Headings';
import { Text } from '../../atoms/Text';

const Menu = ({ years, currentYear, menuState, closeMenu }) => {
	const [ selectedYear, setSelectedYear ] = useState(null);

	const showEpisodes = (event, year) => {
		event.preventDefault();
		setSelectedYear(selectedYear ? null : year);
	};

	const close = (color) => {
		closeMenu(color);
	};

	const currentYearObj = years && years.filter(obj => {
		return obj.title === currentYear
	});
	const currentYearColor = get(currentYearObj, '[0].drawerColor', '#ffb8b1')

	return (
		<MenuWrapper visible={menuState}>
			<div className={menuState ? 'visible' : 'visually-hidden'}>
				<Bg color={currentYearColor}/>
				<MenuInnerWrapper>
					{years && (
						<Nav>
							<H2>Archive by year</H2>
								{years.map(year => (
									<Year key={year._id}>
										<Button selected={currentYear === year.title || selectedYear === year.title} onClick={(event) => showEpisodes(event, year.title)}><Text small>{year.title}</Text></Button>
										<Episodes visible={selectedYear === year.title} className={selectedYear === year.title ? 'visible' : 'visually-hidden'}>
											<H2>Episodes</H2>
											{selectedYear === year.title && (
												<ul>
													{year.episodes && year.episodes.map(episode => (
														<li key={episode._id}>
															<Link to={`/${year.title}${episode.slug.current}`} onClick={(() => close(year.color))}>
																<Text small linkText>{episode.title}</Text>
															</Link>
														</li>
													))}
												</ul>
											)}
										</Episodes>
									</Year>
								))}
						</Nav>
					)}
				</MenuInnerWrapper>
			</div>
		</MenuWrapper>
	)
};

Menu.propTypes = {
	menuState: PropTypes.bool,
};

export default Menu;

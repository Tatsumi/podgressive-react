import styled, {css} from 'styled-components';
import loadingWave from '../../../images/loading-wave.svg';

const WaveWrapper = styled.div`
	max-width: 1200px;
	margin: 60px auto 0;
	
	@media (max-width: 576px) {
  	margin: 120px auto 0;
  	padding: 0;
  }
`;

const Wave= styled.div`
	background-color: rgba(255, 255, 255, 0.1);
	background-image: url(${loadingWave});
	background-repeat: repeat-x; 
	background-position: center center;
	${props => props.painted && css`
		background-image: none;
	`}
`;

const Interact= styled.div`
	margin-bottom: 30px;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	
	button {
		margin-right: 3rem;
		background-color: transparent;
		border: none;
		padding: 0;
		cursor: pointer;
	}
`;

export { WaveWrapper, Wave, Interact};

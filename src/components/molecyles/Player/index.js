import React from 'react';
import ReactDOM from 'react-dom';
import WaveSurfer from 'wavesurfer.js';
import { connect } from 'react-redux';
import { get } from 'lodash';

import play from '../../../images/play.svg';
import pause from '../../../images/pause.svg';
import {Text} from '../../atoms/Text';

import {WaveWrapper, Wave, Interact} from './style';

class Player extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			playingToggle: false,
			isReady: false,
			episodeTitle: null,
			current: '',
			autoPlay: false,
			painted: false,
		};

		this.triggerPlayPause = this.triggerPlayPause.bind(this);
	}

	componentDidMount() {

		this.$el = ReactDOM.findDOMNode(this);
		this.$waveform = this.$el.querySelector('.wave');

		this.wavesurfer = WaveSurfer.create({
			container: this.$waveform,
			waveColor: 'white',
			progressColor: 'black',
			backend: 'MediaElement',
		});

		// this.wavesurfer.on('finish', () => {
		// 	const { episodeTitle } = this.state;
		// 	const next = parseInt(episodeTitle.replace('Episode ', ''), 10) +1;
		// });
	}

	componentDidUpdate() {
		const { episodeTitle, autoPlay } = this.state;
		const { mounted, currentEpisode, updateMountedStateCb } = this.props;

		const mountedTitle = get(mounted, 'title', null);
		const currentEpisodeTitle = get(currentEpisode, 'title', null);

		this.wavesurfer.on('ready', () => {
			if(autoPlay === true) {
				this.wavesurfer.play();
			}
			this.setState({isReady: true});
		});

		this.wavesurfer.on('waveform-ready', () => {
			this.setState({
				painted: true,
			});
		});

		if(episodeTitle === null && currentEpisodeTitle) {
			// Load initial episode by page title.
			this.wavesurfer.load(`http://podgressive.com/episodes/podgressive_ep${currentEpisodeTitle.replace('Episode ', '')}_192kbps.mp3`);
			// Update redux state with title and isPlaying = false.
			updateMountedStateCb(currentEpisodeTitle, false);
			// Update internal state.
			this.setState({
				episodeTitle: currentEpisodeTitle,
				painted: false,
				isReady: false,
			});

		} else if(mountedTitle && episodeTitle !== mountedTitle) {
			// Mount next episode when episode has finished.
			// Load new episode.
			this.wavesurfer.load(`http://podgressive.com/episodes/podgressive_ep${mountedTitle.replace('Episode ', '')}_192kbps.mp3`);
			// Update redux state with title and isPlaying = true.
			updateMountedStateCb(currentEpisodeTitle, true);
			// Update internal state.
			this.setState({
				episodeTitle: mountedTitle,
				autoPlay: true,
				painted: false,
				isReady: false,
			});
		}
	}

	triggerPlayPause(mountedTitle, toggle) {
		this.setState({playingToggle: !toggle});
		this.props.updateMountedStateCb(mountedTitle, !toggle);
		this.wavesurfer.playPause();
	}

	render() {
		const { playingToggle, isReady, painted } = this.state;
		const { mounted, currentEpisode } = this.props;
		const mountedTitle = get(mounted, 'title', null);
		const mountedIsPlaying = get(mounted, 'isPlaying', null);
		const currentEpisodeTitle = get(currentEpisode, 'title', null);

		return (
			<WaveWrapper className='waveform'>
				<Interact>
					{isReady && mountedTitle && (
						<>
							<button onClick={() => this.triggerPlayPause(mountedTitle, playingToggle)}>
								{mountedIsPlaying ? <img src={pause} alt="pause"/> : <img src={play} alt="play"/>}
							</button>
							<Text uppercase>{mountedTitle ? mountedTitle : currentEpisodeTitle}</Text>
						</>
					)}
				</Interact>
				<Wave painted={painted} className="wave"></Wave>
			</WaveWrapper>
		)
	}
}


const mapStateToProps = ({ siteSettings, currentEpisode, yearsFetched, mounted, change }) => {
	return {
		siteSettings: siteSettings.settings,
		currentEpisode: currentEpisode.current,
		years: yearsFetched.years,
		mounted: mounted.episode,
		changeToEpisode: change.toEpisode,
	}
};

export default connect(mapStateToProps, null)(Player);

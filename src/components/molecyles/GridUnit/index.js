import React from 'react';
import Grid from 'styled-components-grid';
import styled from 'styled-components';

const Column = styled.div`
	padding: 2rem;
`;

const GridUnit = ({ size, column, children }) => (
	<Grid.Unit size={size}>
		{column ? (
			<Column>{children}</Column>
		): children}
	</Grid.Unit>
);

GridUnit.defaultProps = {
	size: null,
	column: false,
};
export default GridUnit;

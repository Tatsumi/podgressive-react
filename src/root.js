import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import Index from './pages/Index';
import Year from './pages/Year';
import Episode from './pages/Episode';

const Root = ({ store }) => (
	<Provider store={store}>
		<Router>
			<Switch>
				<Route exact path="/:year/" component={Year} />
				<Route path="/:year/:episode/" component={Episode} />
				<Route exact path="/" component={Index} />
			</Switch>
		</Router>
	</Provider>
);

Root.propTypes = {
	store: PropTypes.object.isRequired
};

export default Root;

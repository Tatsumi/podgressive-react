import { applyMiddleware, createStore, compose} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import rootReducer from './reducers';

// Promise enables handling of async code in redux, thunk enables chained async actions.
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = applyMiddleware(promise, thunk);

export default createStore(rootReducer, composeEnhancers(middleware));
